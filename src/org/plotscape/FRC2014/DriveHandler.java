/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014;

import edu.wpi.first.wpilibj.Gyro;
import edu.wpi.first.wpilibj.RobotDrive;
import edu.wpi.first.wpilibj.SpeedController;
import edu.wpi.first.wpilibj.Talon;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 *
 * @author Joe
 */
public class DriveHandler {
    
    public static DriveHandler self;
    
    public RobotDrive drive;
    
    public SpeedController frontLeft;
    public SpeedController frontRight;
    public SpeedController backLeft;
    public SpeedController backRight;
    
    public Gyro gyro;
    
    public static void init(){
        if(self == null){
            self = new DriveHandler();
            self.frontLeft = new Talon(1);
            self.frontRight = new Talon(2);
            self.backLeft = new Talon(3);
            self.backRight = new Talon(4);
            self.drive = new RobotDrive(self.frontLeft,self.backLeft,self.frontRight,self.backRight);
            //self.drive.setInvertedMotor(RobotDrive.MotorType.kFrontLeft, true);
            //self.drive.setInvertedMotor(RobotDrive.MotorType.kRearLeft, true);
            self.drive.stopMotor();

            self.controllers = new DriveController[0];
            self.gyro = new Gyro(1);
        }
    }
    
    private DriveController[] controllers;
    
    public DriveController[] getControllers(){
        return controllers;
    }
    
    public void setControllers(DriveController[] set){
        controllers = set;
    }
    
    public void setCurrentController(DriveController set){
        current = set;
        if(current!= null){
            current.assignAsController();
        }
    }
    
    private DriveController current;
    
    public void tick(){
        
        if(!MainRun.self.isEnabled()){
            if(current != null){
                current.forceRelease();
            }
            current = null;
            drive.stopMotor();
            return;
        }
        
        if(current == null || current.shouldRelease()){
            
            DriveController replacement = null;
            
            for(int i = 0; i<controllers.length; i++){
                if(controllers[i].readyToUse()){
                    if(replacement == null){
                        replacement = controllers[i];
                    }
                    else
                    {
                        if(replacement.priority() < controllers[i].priority()){
                            replacement = controllers[i];
                        }
                    }
                }
            }
            current = replacement;
            if(current != null){
                current.assignAsController();
            }
        }
        
        if(current == null){
            Log._("Current null!");
        }
        if(current == null || current.shouldRelease() || !current.readyToUse()){
            //Halt the motors
            //Log._("No drive found, halting motors.");
            drive.mecanumDrive_Cartesian(0, 0, 0, 0);
            return;
        }
        
        if(current.resetGyro()){
            gyro.reset();
            current.clearGyroFlag();
        }
        
        current.gyroCallback(gyro.getAngle());
        
        DriveEntry entry = current.getDrive();
        
        if(current.shouldUseGyro()){
            drive.mecanumDrive_Cartesian(entry.getPowerX(), entry.getPowerY(), entry.getRotation(), gyro.getAngle());
        }
        else
        {
            drive.mecanumDrive_Cartesian(entry.getPowerX(), entry.getPowerY(), entry.getRotation(), 0);
        }
        SmartDashboard.putNumber("Front Left", frontLeft.get());
        SmartDashboard.putNumber("Front Right", frontRight.get());
        SmartDashboard.putNumber("Back Left", backLeft.get());
        SmartDashboard.putNumber("Back Right",backRight.get());
    }
    
}
