/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import org.plotscape.FRC2014.MainRun;
import org.plotscape.FRC2014.UiInterface;

/**
 *
 * @author Joe
 */
public class ProngRunner extends Thread {
    
    public static ProngRunner self;
    
    public static void init(){
        self = new ProngRunner();
    }
    
    public static void startP(){
        if(!self.isAlive()){
            self.start();
        }
    }
    
    public static void stopP(){
        if(self.isAlive()){
            //self.interrupt();
        }
    }
        public void run(){
        while(MainRun.self.isEnabled()){
            long startingTime = System.currentTimeMillis();
            if(UiInterface.getB()){
                System.out.println("b");
                ProngsRunnable.left.extend(false);
                ProngsRunnable.right.extend(false);
            }
            else if(UiInterface.getA()){
                System.out.println("a");
                ProngsRunnable.left.contract(false);
                ProngsRunnable.right.contract(false);
            }
            long shouldDelay = 50 - (System.currentTimeMillis() - startingTime);
            if(shouldDelay > 0){
                try {
                    Thread.sleep(shouldDelay);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    
}
