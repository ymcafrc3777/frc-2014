/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import org.plotscape.FRC2014.DriveHandler;
import org.plotscape.FRC2014.MainRun;

/**
 *
 * @author Joe
 */
public class DriveRunnable extends Thread{

    private static DriveRunnable self;
    
    public static DriveRunnable getRunnable(){
        return self;
    }
    
    public static void init(){
         self = new DriveRunnable();
    }
    
    public static void startP(){
            self.start();
    }
    
    public static void stopP(){
        if(self.isAlive()){
            //self.interrupt();
        }
    }

    public void run() {
        while(MainRun.self.isEnabled()){
            long startingTime = System.currentTimeMillis();
            try{
                DriveHandler.self.tick();
            }
            catch(Exception e){
                e.printStackTrace();
            }
            long shouldDelay = 10 - (System.currentTimeMillis() - startingTime);
            if(shouldDelay > 0){
                try {
                    Thread.sleep(shouldDelay);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    
    }
}
