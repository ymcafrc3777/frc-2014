/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import org.plotscape.FRC2014.Log;

/**
 *
 * @author Joe
 */
public class ProngsRunnable{
        public static ProngsRunnable right = new ProngsRunnable();
        public static ProngsRunnable left = new ProngsRunnable();
    
    private int state = 0; //0=Lowered, 1=Raising, 2=Lowering, 3=Raised
    
    private void setState(int i){
        this.state = i;
        Log._("Cylinder set to "+i);
    }
    
    
    private DoubleSolenoid valve = null;
    
    
    public static void init(){
        right = new ProngsRunnable();
        left = new ProngsRunnable();
        right.init(3,4);
        left.init(5,6);
    }
    
    public void init(int open, int close){
        this.state = 0;
        this.valve = new DoubleSolenoid(open,close);
    }
    
    
    public boolean extending(){
        return this.state == 1 || this.state == 3;
    }
    
    public void extend(boolean timeout){
        this.valve.set(DoubleSolenoid.Value.kForward);
        setState(1);
        if(timeout){
            new Thread(new ExtendRunnable(this)).start();
        }
    }
    
    public void contract(boolean timeout){
        this.valve.set(DoubleSolenoid.Value.kReverse);
        setState(2);
        if(timeout){
            new Thread(new ContractRunnable(this)).start();
        }
    }

    private static class ExtendRunnable implements Runnable{

        ProngsRunnable self;
        public ExtendRunnable(ProngsRunnable runnable){
            self = runnable;
        }
        
        public void run(){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
            self.valve.set(DoubleSolenoid.Value.kOff);
            self.setState(3);
        }
    }
    private static class ContractRunnable implements Runnable{

        
        ProngsRunnable self;
        public ContractRunnable(ProngsRunnable runnable){
            self = runnable;
        }
        
        public void run(){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
            self.valve.set(DoubleSolenoid.Value.kOff);
            self.setState(0);
        }
    }
}
