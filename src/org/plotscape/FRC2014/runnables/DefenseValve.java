/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import org.plotscape.FRC2014.Log;
import org.plotscape.FRC2014.MainRun;
import org.plotscape.FRC2014.UiInterface;

/**
 *
 * @author Joe
 */
public class DefenseValve extends Thread{
    
    public static DefenseValve self;
    
    private static int state = 0; //0=Lowered, 1=Raising, 2=Lowering, 3=Raised
    
    private static void setState(int i){
        DefenseValve.state = i;
        Log._("Cylinder set to "+i);
    }
    
    
    private static DoubleSolenoid valve = null;
    
    public static void startP(){
            self.start();
        
    }
    
    public static void stopP(){
        if(self.isAlive()){
            //self.interrupt();
        }
    }
    
    public static void init(){
        self = new DefenseValve();

    }
    
    private static void valveInit(){
       if(DefenseValve.valve == null){
            DefenseValve.valve = new DoubleSolenoid(1,2);
        }
    }
    
    public static boolean raising(){
        return DefenseValve.state == 1 || DefenseValve.state == 3;
    }

    
    public void run(){
        while(MainRun.self.isEnabled()){
            long startingTime = System.currentTimeMillis();
            if(UiInterface.getJoystickButton(3) && !raising()){
                raise(true);
            }
            else if(UiInterface.getJoystickButton(2) && raising()){
                 lower(true);
            }
            long shouldDelay = 50 - (System.currentTimeMillis() - startingTime);
            if(shouldDelay > 0){
                try {
                    Thread.sleep(shouldDelay);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    public static void raise(boolean timeout){
        valveInit();
        DefenseValve.valve.set(DoubleSolenoid.Value.kForward);
        setState(1);
        if(timeout){
            new Thread(new RaiseRunnable()).start();
        }
    }
    
    public static void lower(boolean timeout){
        valveInit();
        DefenseValve.valve.set(DoubleSolenoid.Value.kReverse);
        setState(2);
        if(timeout){
            new Thread(new LowerRunnable()).start();
        }
    }

    private static class RaiseRunnable implements Runnable{

        public void run(){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
            DefenseValve.valve.set(DoubleSolenoid.Value.kOff);
            setState(3);
        }
    }
    private static class LowerRunnable implements Runnable{

        public void run(){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
                return;
            }
            DefenseValve.valve.set(DoubleSolenoid.Value.kOff);
            setState(0);
        }
    }
    
}
