/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import org.plotscape.FRC2014.MainRun;

/**
 *
 * @author Joe
 */
public class PIDInputRunnable extends Thread{
        private static PIDInputRunnable self;
    
    private PIDController controller;
    
    private PIDInputRunnable(PIDController controller){
        this.controller = controller;
    }
        
        
    public static void init(PIDController controller){
            self = new PIDInputRunnable(controller);
        
            self.start();
        
    }
    
    public static PIDInputRunnable getRunnable(){
        return self;
    }

    
    public void run(){
      while(MainRun.self.isEnabled()){
            long startingTime = System.currentTimeMillis();
            try{
                SmartDashboard.putData("PID", controller);
            }
            catch(Exception e){
                
            }
            long shouldDelay = 500 - (System.currentTimeMillis() - startingTime);
            if(shouldDelay > 0){
                try {
                    Thread.sleep(shouldDelay);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
}
