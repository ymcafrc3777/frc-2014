/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.runnables;

import edu.wpi.first.wpilibj.PIDSource;
import org.plotscape.FRC2014.CircleEntry;
import org.plotscape.FRC2014.MainRun;

/**
 *
 * @author Joe
 */
public class CircleRunnable extends Thread implements PIDSource{
    
    private static CircleRunnable self;
    
    private double output = 0;
    
    
    public static void init(){
            self = new CircleRunnable();
        
    }
    
    public static CircleRunnable getRunnable(){
        return self;
    }

    public double pidGet() {
        return output;
    }
    

    public static void startP(){
            self.start();
    }
    
    public static void stopP(){
        if(self.isAlive()){
            //self.interrupt();
        }
    }
    
    public void run(){
      while(MainRun.self.isEnabled()){
            long startingTime = System.currentTimeMillis();
            CircleEntry entry = CircleEntry.grabFromTable();
            double imageSize = 0;
            if(MainRun.table.containsKey("IMAGE_WIDTH")){
                imageSize = MainRun.table.getNumber("IMAGE_WIDTH");
            }
            else
            {
                imageSize = 320;
            }
            if(entry != null){
 
                output = (entry.x-(imageSize/2))/(imageSize/2);
            }
            else
            {
                output = 0;
            }
            long shouldDelay = 100 - (System.currentTimeMillis() - startingTime);
            if(shouldDelay > 0){
                try {
                    Thread.sleep(shouldDelay);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
    }
    
    
    
}
