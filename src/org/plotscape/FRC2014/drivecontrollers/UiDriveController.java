/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.drivecontrollers;

import org.plotscape.FRC2014.DriveController;
import org.plotscape.FRC2014.DriveEntry;
import org.plotscape.FRC2014.Log;
import org.plotscape.FRC2014.MainRun;
import org.plotscape.FRC2014.UiInterface;

/**
 *
 * @author Joe
 */
public class UiDriveController implements DriveController {

    public boolean readyToUse() {
        return MainRun.self.isEnabled() && MainRun.self.isOperatorControl();
    }

    public boolean shouldUseGyro() {
        return UiInterface.getRightLowerSholder();
    }

    public DriveEntry getDrive() {
        
        double in = UiInterface.getRightX();
        
        double out = 0;
        
        if(in < .75){
            out = (.5/.75) * in;
        }
        else
        {
            out = .5 + ((in - .75)*2);
        }
        
        return new DriveEntry(
        UiInterface.getLeftX(),
        UiInterface.getLeftY(),
        out);
    }

    public boolean shouldRelease() {
        if(UiInterface.getJoystickButton(8)){
            return true;
        }
        return false;

    }
    
    public void forceRelease(){
        Log._("Releasing Ui Drive Controller");
    }

    public int priority() {
        return 0;
    }

    public void gyroCallback(double gyroAngle) {
        
    }

    public boolean resetGyro() {
        return UiInterface.getY();
    }

    public void clearGyroFlag() {
        
    }

    public void assignAsController() {
        Log._("Starting Ui Drive Controller");
    }
    
}
