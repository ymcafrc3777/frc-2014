/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.drivecontrollers;

import org.plotscape.FRC2014.DriveController;
import org.plotscape.FRC2014.DriveEntry;
import org.plotscape.FRC2014.MainRun;

/**
 *
 * @author Joe
 */
public class TestReflector implements DriveController{
    
    public static TestReflector self = new TestReflector();

    public boolean readyToUse() {
        return MainRun.self.isTest();
    }

    public boolean shouldUseGyro() {
        return true;
    }

    public static DriveEntry entry = new DriveEntry(0,0,0);
    
    public DriveEntry getDrive() {
        return entry;
    }

    public boolean shouldRelease() {
        return !MainRun.self.isTest();
    }

    public int priority() {
        return 0; //Should be only Drive controller
    }

    public void gyroCallback(double gyroAngle) {
        
    }

    public boolean resetGyro() {
        return false;
    }

    public void clearGyroFlag() {
       
    }

    public void assignAsController() {
       
    }

    public void forceRelease() {
        
    }
    
}
