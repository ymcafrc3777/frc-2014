/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014.drivecontrollers;

import edu.wpi.first.wpilibj.PIDOutput;
import org.plotscape.FRC2014.runnables.DefenseValve;
import org.plotscape.FRC2014.DriveController;
import org.plotscape.FRC2014.DriveEntry;
import org.plotscape.FRC2014.Log;
import org.plotscape.FRC2014.UiInterface;

/**
 *
 * @author Joe
 */
public class DefensePIDCrossover implements DriveController, PIDOutput{

    private double output;
    
    private boolean lockEnable = false;
    public boolean readyToUse() {
        return UiInterface.getJoystickButton(8) || lockEnable;
    }

    public boolean shouldUseGyro() {
        return true;
    }

    public DriveEntry getDrive() {
        double raw = 0;
        if(UiInterface.getJoystickButton(1)){   //Trigger
            if(UiInterface.getJoystickButton(5)){ //Left head button
                raw = -.5;
            }
            else if(UiInterface.getJoystickButton(4)){ //Right head button
                raw = .5;
            }
        }
        else
        {
           raw = output;
        }
        double out = Math.max(-1,Math.min(1,raw));
        double uiX = UiInterface.getJoystickX()*.5;
        double uiY = UiInterface.getJoystickY()*.5;
        return new DriveEntry(
            uiX,
            out,
            uiY);
    }

    public boolean shouldRelease() {
        if(UiInterface.getJoystickButton(9)){

            return true;
        }
        return false;
    }
    
    public void forceRelease(){
        Log._("Releasing Defense PID Enhanced Controller");
        if(DefenseValve.raising()){
            DefenseValve.lower(true);
        }
        lockEnable = false;
    }

    public int priority() {
        return 1;
    }

    public void gyroCallback(double gyroAngle) {
        
    }

    boolean shouldReset;
    
    public boolean resetGyro() {
        return shouldReset || UiInterface.getJoystickButton(6);
    }

    public void clearGyroFlag() {
        shouldReset = false;
    }

    public void assignAsController() {
        shouldReset = true;
        Log._("Starting Defense PID Enhanced Controller");
        lockEnable = true;
    }

    public void pidWrite(double output) {
        this.output = output;
    }
    
    
}
