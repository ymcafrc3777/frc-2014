/*----------------------------------------------------------------------------*/
/* Copyright (c) FIRST 2008. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package org.plotscape.FRC2014;


import org.plotscape.FRC2014.drivecontrollers.TestReflector;
import org.plotscape.FRC2014.runnables.DefenseValve;
import org.plotscape.FRC2014.drivecontrollers.UiDriveController;
import org.plotscape.FRC2014.drivecontrollers.DefensePIDCrossover;
import org.plotscape.FRC2014.runnables.DriveRunnable;
import org.plotscape.FRC2014.runnables.CircleRunnable;
import org.plotscape.FRC2014.runnables.PIDInputRunnable;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.SimpleRobot;
import edu.wpi.first.wpilibj.networktables.NetworkTable;
import org.plotscape.FRC2014.runnables.ProngRunner;
import org.plotscape.FRC2014.runnables.ProngsRunnable;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the SimpleRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class MainRun extends SimpleRobot {
    
    
    
    /**
     * This function is called once each time the robot enters autonomous mode.
     */
    
    
    public static MainRun self;
    
    public static NetworkTable table;
    
    private Compressor compressor;
    
    public static PIDController defenseController;
    
    public void robotInit(){
        self = this;
        table = NetworkTable.getTable("SmartDashboard");
        compressor = new Compressor(1,1);

        CircleRunnable.init();
        CircleRunnable.getRunnable().setPriority(Thread.NORM_PRIORITY);
        
        UiInterface.init();
        DriveHandler.init();
        DriveController[] controllers = new DriveController[2];
        controllers[0] = new UiDriveController();
        controllers[1] = new DefensePIDCrossover();
        DriveHandler.self.setControllers(controllers);
        DriveRunnable.init();
        DriveRunnable.getRunnable().setPriority(Thread.MAX_PRIORITY);
        
        
        defenseController = new PIDController(.4,.01,0,CircleRunnable.getRunnable(), (PIDOutput) controllers[1]);
        defenseController.setSetpoint(0);
        defenseController.setContinuous(false);
        defenseController.setInputRange(-1, 1);
        defenseController.setAbsoluteTolerance(.05);
        defenseController.setOutputRange(-1, 1);
        defenseController.enable();

        ProngRunner.init();
        ProngsRunnable.init();
        
        PIDInputRunnable.init(defenseController);
        
        DefenseValve.init();
       
    }
    
    public void autonomous() {
        compressor.start();
        CircleRunnable.init();
        CircleRunnable.startP();

        DriveHandler.self.setCurrentController(DriveHandler.self.getControllers()[1]);  //Defense Controller
        
                DriveRunnable.init();
        DriveRunnable.startP();
        //defenseController.setOutputRange(1, 1);
        CircleEntry.setSizeLimit(40);
        DefenseValve.init();
        DefenseValve.raise(true);
        //DefenseValve.startP(); No user interaction needed
    }

    /**
     * This function is called once each time the robot enters operator control.
     */
    
    public void operatorControl() {
        compressor.start();
        CircleRunnable.init();
        CircleRunnable.startP();
        DefenseValve.init();
        DefenseValve.startP();

        ProngRunner.init();
        ProngRunner.startP();
        DriveHandler.self.setCurrentController(DriveHandler.self.getControllers()[0]);
        
                DriveRunnable.init();
        DriveRunnable.startP();
    }
    
    /**
     * This function is called once each time the robot enters test mode.
     */
    
    Thread testing = null;
    
    public void test() {
        compressor.start();
        CircleRunnable.stopP();
        DriveRunnable.startP();
        DefenseValve.startP();
        ProngsRunnable.left.extend(true);
        ProngsRunnable.right.extend(true);
        DriveHandler.self.setCurrentController(TestReflector.self);
        Runnable test = new Runnable(){
            public void run() {
                try{
                    TestReflector.entry.set(.3, 0, 0);
                    Thread.sleep(1000);
                    TestReflector.entry.set(-.3,0,0);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0, .2,0);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0, -.2, 0);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0, 0, .2);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0, 0, -.2);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0,0, .2);
                    Thread.sleep(1000);
                    TestReflector.entry.set(0, 0, 0);
                    DefenseValve.raise(true);
                    Thread.sleep(5000);
                    DefenseValve.lower(true);
                    Thread.sleep(5000);
                    ProngsRunnable.right.extend(true);
                    ProngsRunnable.left.extend(true);
                    Thread.sleep(1000);
                    ProngsRunnable.right.contract(true);
                    ProngsRunnable.left.contract(true);
                    Thread.sleep(1000);
                }
                catch(Exception e){
                    
                }
            }
        };
        
        testing = new Thread(test);
        testing.start();
    }
    
    public void disabled(){
        //DriveRunnable.shouldRun = false;
        compressor.stop();
    }
}
