/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014;

import edu.wpi.first.wpilibj.Joystick;

/**
 *
 * @author Joe
 */
public class UiInterface {
    
    private static Joystick gamepad = null;
    
    private static Joystick joystick = null;
    
    public static void init(){
        if(gamepad == null){
            gamepad = new Joystick(1);
        }
        if(joystick == null){
            joystick = new Joystick(2);
        }
    }
    
    public static double getLeftX(){
        return gamepad.getRawAxis(1);
    }
    
    public static double getLeftY(){
        return gamepad.getRawAxis(2);
    }
    
    public static double getRightX(){
        return gamepad.getRawAxis(3);
    }
    
    public static double getRightY(){
        return gamepad.getRawAxis(4);
    }
    
    public static boolean getY(){
        return gamepad.getRawButton(4);
    }
    
    public static boolean getB(){
        return gamepad.getRawButton(3);
    }
    
    public static boolean getA(){
        return gamepad.getRawButton(2);
    }
    
    public static boolean getRightLowerSholder(){
        return gamepad.getRawButton(8);
    }
    
    public static double getJoystickX(){
        return joystick.getRawAxis(1);
    }
    
    public static double getJoystickY(){
        return joystick.getRawAxis(2);
    }
    
    public static boolean getJoystickButton(int i){
        return joystick.getRawButton(i);
    }
}
