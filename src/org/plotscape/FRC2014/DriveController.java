/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014;

/**
 *
 * @author Joe
 */
public interface DriveController {
    
    public boolean readyToUse();
    
    public boolean shouldUseGyro();
    
    public DriveEntry getDrive();
    
    public boolean shouldRelease();
    
    public int priority();
    
    public void gyroCallback(double gyroAngle);
    
    public boolean resetGyro();
    
    public void clearGyroFlag();
    
    public void assignAsController();
    
    public void forceRelease();
    
}
