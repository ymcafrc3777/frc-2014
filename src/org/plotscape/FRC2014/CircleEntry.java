/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014;

import edu.wpi.first.wpilibj.networktables2.type.NumberArray;

/**
 *
 * @author Joe
 */
public class CircleEntry {
    
    public static int sizeMin = 20;
    
    public static int getSizeLimit(){
        return sizeMin;
    }
    
    public static void setSizeLimit(int size){
        sizeMin = size;
    }
    
    public static CircleEntry grabFromTable(){
        final NumberArray circles = new NumberArray();
        if(MainRun.table.containsKey("CIRCLES")){
            MainRun.table.retrieveValue("CIRCLES",circles);
            if(circles.size() > 2){
                    double x = circles.get(0);
                    double y = circles.get(1);
                    double rad = circles.get(2);
                    return new CircleEntry(x,y,rad);
            }
            else
            {
                return null;
            }
        }
        return null;

    }
    
    public CircleEntry(double x, double y, double rad){
        this.x = x;
        this.y = y;
        this.rad = rad;
    }
    
    public double x;
    public double y;
    public double rad;
}
