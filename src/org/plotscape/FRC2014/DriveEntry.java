/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.plotscape.FRC2014;

/**
 *
 * @author Joe
 */
public class DriveEntry {
    
    public DriveEntry(double powerX, double powerY, double rotation){
        this.powerX = powerX;
        this.powerY = powerY;
        this.rotation = rotation;
    }
    
    private double powerX;
    private double powerY;
    
    private double rotation;

    public double getPowerX() {
        return powerX;
    }

    public double getPowerY() {
        return powerY;
    }

    public double getRotation() {
        return rotation;
    }
    
    public void set(double x, double y, double rot){
        powerX = x;
        powerY = y;
        rotation = rot;
    }
    
}
